/*
 * This file is generated by jOOQ.
 */
package net.swigg.indexcellent.tables;


import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import net.swigg.indexcellent.DefaultSchema;
import net.swigg.indexcellent.Indexes;
import net.swigg.indexcellent.Keys;
import net.swigg.indexcellent.tables.records.JobRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Job extends TableImpl<JobRecord> {

    private static final long serialVersionUID = -808678309;

    /**
     * The reference instance of <code>job</code>
     */
    public static final Job JOB = new Job();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<JobRecord> getRecordType() {
        return JobRecord.class;
    }

    /**
     * The column <code>job.uuid</code>.
     */
    public final TableField<JobRecord, String> UUID = createField("uuid", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>job.name</code>.
     */
    public final TableField<JobRecord, String> NAME = createField("name", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>job.created</code>.
     */
    public final TableField<JobRecord, Timestamp> CREATED = createField("created", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "");

    /**
     * The column <code>job.snapshot</code>.
     */
    public final TableField<JobRecord, Boolean> SNAPSHOT = createField("snapshot", org.jooq.impl.SQLDataType.BOOLEAN, this, "");

    /**
     * Create a <code>job</code> table reference
     */
    public Job() {
        this(DSL.name("job"), null);
    }

    /**
     * Create an aliased <code>job</code> table reference
     */
    public Job(String alias) {
        this(DSL.name(alias), JOB);
    }

    /**
     * Create an aliased <code>job</code> table reference
     */
    public Job(Name alias) {
        this(alias, JOB);
    }

    private Job(Name alias, Table<JobRecord> aliased) {
        this(alias, aliased, null);
    }

    private Job(Name alias, Table<JobRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Job(Table<O> child, ForeignKey<O, JobRecord> key) {
        super(child, key, JOB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.SQLITE_AUTOINDEX_JOB_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<JobRecord> getPrimaryKey() {
        return Keys.PK_JOB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<JobRecord>> getKeys() {
        return Arrays.<UniqueKey<JobRecord>>asList(Keys.PK_JOB);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Job as(String alias) {
        return new Job(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Job as(Name alias) {
        return new Job(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Job rename(String name) {
        return new Job(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Job rename(Name name) {
        return new Job(name, null);
    }
}
