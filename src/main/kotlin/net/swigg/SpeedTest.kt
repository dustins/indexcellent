//package net.swigg
//
//import kotlinx.coroutines.delay
//import kotlinx.coroutines.launch
//import kotlinx.coroutines.runBlocking
//import kotlinx.coroutines.time.withTimeout
//import kotlinx.coroutines.yield
//import org.jetbrains.exposed.dao.EntityID
//import org.jetbrains.exposed.dao.IdTable
//import org.jetbrains.exposed.dao.UUIDEntity
//import org.jetbrains.exposed.dao.UUIDTable
//import org.jetbrains.exposed.sql.*
//import org.jetbrains.exposed.sql.transactions.transaction
//import java.time.Duration
//import java.util.*
//import kotlin.system.measureTimeMillis
//
//interface SpeedTest {
//    suspend fun run(): Unit
//
//    fun data(): Sequence<Triple<UUID, String, String>> = sequence {
//        while (true) {
//            yield(Triple(UUID.randomUUID(), UUID.randomUUID().toString(), UUID.randomUUID().toString()))
//        }
//    }
//}
//
//class Exposed : SpeedTest {
//    init {
//        Database.connect("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
//        transaction {
//            SchemaUtils.create(IndexFiles)
//        }
//    }
//
//    override suspend fun run() {
//        try {
//            withTimeout(Duration.ofSeconds(5)) {
//                launch {
//                    while (true) {
//                        transaction {
//                            IndexFiles.batchInsert(data().take(50).asIterable()) {
//                                this[IndexFiles.id] = EntityID(it.first, IndexFiles)
//                                this[IndexFiles.name] = it.second
//                                this[IndexFiles.hash] = it.third
//                            }
//                        }
//                        yield()
//                    }
//                }
//            }
//        } catch (t: Throwable) {
//            println("timed out")
//        }
//
//        transaction {
//            println("Inserted ${IndexFiles.selectAll().count()}")
//        }
//    }
//
//    class IndexFile(id: EntityID<UUID>) : UUIDEntity(id) {}
//
//    object IndexFiles : UUIDTable(name = "index_file") {
//        val name = varchar("name", 255)
//        val hash = varchar("hash", 64)
//    }
//}
//
//fun main() = runBlocking<Unit> {
//    val exposed = Exposed()
//    val time = measureTimeMillis { exposed.run() }
//    println("took ${time}ms")
//}