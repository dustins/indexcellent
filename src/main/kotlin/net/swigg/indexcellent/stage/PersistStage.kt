package net.swigg.indexcellent.stage

import com.codahale.metrics.Counter
import com.codahale.metrics.MetricRegistry
import net.swigg.indexcellent.WorkflowFile
import net.swigg.indexcellent.tables.File.*
import net.swigg.indexcellent.tables.Job_File.*
import org.jooq.DSLContext
import java.nio.file.attribute.FileTime
import java.sql.Timestamp

fun FileTime.toTimestamp(): Timestamp = Timestamp(this.toMillis())

class PersistStage(metrics: MetricRegistry, val dsl: DSLContext) : Stage {
    private val log = mu.KotlinLogging.logger {}

    private val persistedCounter: Counter = metrics.counter(MetricRegistry.name(this::class.java, "persisted"))

    override suspend fun process(workflowFile: WorkflowFile) {
        try {
            val job = workflowFile.indexJob
            val file = workflowFile.indexItem

            dsl.insertInto(FILE)
                .columns(
                    FILE.UUID, FILE.PATH, FILE.DEPTH, FILE.NAME, FILE.HASH,
                    FILE.TYPE, FILE.SIZE, FILE.CREATED, FILE.MODIFIED
                )
                .values(
                    file.uuid.toString(),
                    file.path.toString(),
                    file.path.nameCount,
                    file.path.fileName.toString(),
                    file.hash.toString(),
                    file.type.toString(),
                    workflowFile.attributes.size().toBigInteger(),
                    workflowFile.attributes.creationTime().toTimestamp(),
                    workflowFile.attributes.lastModifiedTime().toTimestamp()
                )
                .onConflictDoNothing()
                .execute()

            dsl.insertInto(JOB__FILE)
                .columns(JOB__FILE.JOB__UUID, JOB__FILE.FILE__UUID)
                .values(job.uuid.toString(), file.uuid.toString())
                .execute()

            persistedCounter.inc()
        } catch (t: Throwable) {
            log.error("Unable to insert file.", t)
        }
    }
}