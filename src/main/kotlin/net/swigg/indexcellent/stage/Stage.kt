package net.swigg.indexcellent.stage

import net.swigg.indexcellent.WorkflowFile

interface Stage {
    suspend fun process(workflowFile: WorkflowFile)
    suspend fun cleanup() {}
}