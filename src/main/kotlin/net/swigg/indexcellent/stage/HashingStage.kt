package net.swigg.indexcellent.stage

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.MetricRegistry.name
import com.google.common.hash.HashFunction
import com.google.common.hash.Hashing
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.time.delay
import kotlinx.coroutines.yield
import net.swigg.indexcellent.WorkflowFile
import java.time.Duration

class HashingStage(metrics: MetricRegistry) : Stage {
    private val log = mu.KotlinLogging.logger {}

    private val timer = metrics.timer(name(this::class.java, "process"))

    override suspend fun process(workflowFile: WorkflowFile) {
        timer.time().use {
            val hashCode = hashingFunction.newHasher()
            val byteArray = ByteArray(byteArraySize)

            coroutineScope {
                val watchdog = launch {
                    delay(watchdogDuration)
                    log.warn("WorkflowFile (${workflowFile.indexItem.path}) is taking long to hash.")
                }

                launch {
                    workflowFile.inputStream().use { inputStream ->
                        do {
                            val length = inputStream.read(byteArray)

                            if (length > 0) {
                                hashCode.putBytes(byteArray, 0, length)
                            }

                            if (inputStream.available() % yieldSize == 0) {
                                yield()
                            }
                        } while (length != -1)
                    }

                    workflowFile.indexItem.hash = hashCode.hash()
                    watchdog.cancel()
                }
            }
        }
    }

    companion object {
        const val byteArraySize: Int = 1 shl 12
        const val yieldSize: Int = 1 shl 16

        val watchdogDuration: Duration = Duration.ofSeconds(5)
        val hashingFunction: HashFunction = Hashing.sha256()
    }
}