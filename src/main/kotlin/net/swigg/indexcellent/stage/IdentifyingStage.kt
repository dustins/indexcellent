package net.swigg.indexcellent.stage

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.MetricRegistry.name
import com.codahale.metrics.Timer
import net.swigg.indexcellent.WorkflowFile
import org.apache.tika.detect.DefaultDetector
import org.apache.tika.metadata.Metadata
import java.util.concurrent.ConcurrentLinkedQueue

class IdentifyingStage(metrics: MetricRegistry) : Stage {
    private val log = mu.KotlinLogging.logger {}

    private val timer: Timer = metrics.timer(name(this::class.java, "process"))

    override suspend fun process(workflowFile: WorkflowFile) {
        timer.time().use {
            val metadata = metadataQueue.poll() ?: Metadata()
            val indexFile = workflowFile.indexItem

            metadata.names().forEach(metadata::remove)
            metadata.set(Metadata.RESOURCE_NAME_KEY, indexFile.path.fileName.toString())
            indexFile.type = detector.detect(workflowFile.inputStream(), metadata)
            metadataQueue.offer(metadata)
        }
    }

    companion object {
        val detector = DefaultDetector()
        val metadataQueue = ConcurrentLinkedQueue(mutableListOf<Metadata>())
    }
}