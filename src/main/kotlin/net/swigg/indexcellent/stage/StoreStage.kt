package net.swigg.indexcellent.stage

import com.codahale.metrics.Counter
import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.MetricRegistry.name
import com.codahale.metrics.Timer
import net.swigg.indexcellent.WorkflowFile
import java.nio.file.FileAlreadyExistsException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class StoreStage(val destination: Path, metrics: MetricRegistry) : Stage {
    private val log = mu.KotlinLogging.logger {}

    private val timer: Timer = metrics.timer(name(this::class.java, "process"))
    private val storeCounter: Counter = metrics.counter(name(this::class.java, "stored"))

    init {
        val destinationFile = destination.toFile()
        if (!destinationFile.exists()) {
            destinationFile.mkdirs()
        }
    }

    override suspend fun process(workflowFile: WorkflowFile) {
        timer.time().use {
            workflowFile.indexItem.hash?.let { hashCode ->
                val hash = hashCode.toString()

                val components = hash.split(regex, limit = 3)
                val target = destination.resolve(Paths.get(components[0], components[1], hash))
                target.parent.toFile().mkdirs()

                try {
                    Files.copy(workflowFile.inputStream(), target)
                    storeCounter.inc()
                } catch (ex: FileAlreadyExistsException) {
                    // if the file already exists don't worry about it
                }
            } ?: throw RuntimeException("File `${workflowFile.indexItem.path}` has no hash.")
        }
    }

    companion object {
        val regex = Regex("(?<=\\G.{3})")
    }
}