package net.swigg.indexcellent

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.MetricRegistry.name
import com.google.common.base.Preconditions
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import net.swigg.indexcellent.stage.Stage
import net.swigg.indexcellent.tables.Job.JOB
import org.jooq.DSLContext
import java.nio.channels.FileChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardOpenOption.READ
import java.nio.file.attribute.BasicFileAttributes
import java.sql.Timestamp
import java.time.Duration
import java.time.Instant
import kotlin.system.measureTimeMillis

class IndexcellentEngine(
    val stages: List<Stage>,
    val dsl: DSLContext,
    metrics: MetricRegistry
) {
    private val log = mu.KotlinLogging.logger {}

    private val processTimer = metrics.timer(name(this::class.java, "index.files-timer"))
    private val processCounter = metrics.counter(name(this::class.java, "index.files-processed"))

    fun index(name: String, location: Path) = runBlocking<Unit> {
        val totalTime = measureTimeMillis {
            val job = IndexJob(name, location)
            log.info("Running job `${job.name}` for `${job.path}`")

            val file = location.toFile()
            Preconditions.checkArgument(file.exists(), "Location '$location' does not exist.")
            Preconditions.checkArgument(file.isDirectory, "Location '$location' is not a directory.")

            indexJob(job)
        }

        log.info("Indexed $name ($location) in ${Duration.ofMillis(totalTime)}")
    }

    private suspend fun indexJob(job: IndexJob) {
        val concurrency = Runtime.getRuntime().availableProcessors().coerceAtLeast(2)

        dsl.insertInto(JOB)
            .columns(JOB.UUID, JOB.NAME, JOB.CREATED, JOB.SNAPSHOT)
            .values(job.uuid.toString(), job.name, Timestamp(Instant.now().toEpochMilli()), false)
            .execute()

        coroutineScope {
            val statusJob = launch {
                while (true) {
                    delay(10_000)
                    log.info("Processed: ${processCounter.count}")
                }
            }

            val pathTree = job.path.toFile().walkBottomUp()//.filter(File::isFile)
                .map { it.canonicalFile.toPath().toRealPath() }
                .filter { it.startsWith(job.path) }

            val paths = produce(Dispatchers.Default, concurrency) { pathTree.forEach { send(it) } }
            List(concurrency) {
                launch(Dispatchers.Default) {
                    processFiles(paths, job)
                }
            }.forEach { it.join() }

            statusJob.cancel()
        }
    }

    private suspend fun processFiles(paths: ReceiveChannel<Path>, job: IndexJob) {
        for (path in paths) {
            processCounter.inc()
            processTimer.time().use {
                val attributes = Files.readAttributes(path, BasicFileAttributes::class.java)
                val indexItem = createIndexItem(job.path.relativize(path), attributes)
                val workflowFile = createWorkflowFile(path, attributes, indexItem, job)
                workflowFile.use {
                    stages.forEach {
                        val stageTime = measureTimeMillis {
                            it.process(workflowFile)
                        }

                        workflowFile.stageDuration.put(it::class, Duration.ofMillis(stageTime))
                    }
                }

                coroutineScope<Unit> {
                    stages.map { launch { it.cleanup() } }
                        .forEach { it.join() }
                }
            }
        }
    }

    private fun createWorkflowFile(
        path: Path,
        attributes: BasicFileAttributes,
        indexItem: IndexItem,
        job: IndexJob
    ): WorkflowFile {
        return when (indexItem) {
            is IndexItem.IndexFile -> {
                val channel = FileChannel.open(path, READ)
                WorkflowFile(channel, attributes, indexItem, job)
            }
            else -> WorkflowFile(attributes = attributes, indexItem = indexItem, indexJob = job)
        }
    }

    private fun createIndexItem(path: Path, attributes: BasicFileAttributes): IndexItem? {
        return when {
            attributes.isRegularFile -> IndexItem.IndexFile(path)
            attributes.isDirectory -> IndexItem.IndexDirectory(path)
            attributes.isSymbolicLink -> {
                log.warn("Unhandled symbolic link. {}", path)
                return null
            }
            attributes.isOther -> {
                log.warn("Unhandled other file. {}", path)
                return null
            }
            else -> {
                log.warn("Unknown file type.")
                return null
            }
        }
    }
}