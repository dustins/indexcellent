package net.swigg.indexcellent

import com.google.common.hash.HashCode
import org.apache.tika.mime.MediaType
import java.nio.file.Path
import java.util.*

sealed class IndexItem(
    val path: Path,
    val type: MediaType? = null,
    val uuid: UUID = UUID.randomUUID()
) {
    class IndexDirectory(
        path: Path,
        uuid: UUID = UUID.randomUUID()
    ) : IndexItem(path, MediaType.parse("inode/directory"), uuid)

    class IndexFile(
        path: Path,
        var hash: HashCode? = null,
        type: MediaType? = null,
        uuid: UUID = UUID.randomUUID()
    ) : IndexItem(path, type, uuid)
}