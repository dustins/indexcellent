package net.swigg.indexcellent

import com.codahale.metrics.MetricRegistry
import net.swigg.indexcellent.stage.HashingStage
import net.swigg.indexcellent.stage.IdentifyingStage
import net.swigg.indexcellent.stage.PersistStage
import net.swigg.indexcellent.stage.StoreStage
import org.jooq.Configuration
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DefaultConfiguration
import org.jooq.util.sqlite.SQLiteDSL
import org.koin.dsl.module
import java.sql.DriverManager

val indexcellentKoinModule = module {
    single<Configuration>(createdAtStart = true) {
        DefaultConfiguration().also {
            it.setSQLDialect(SQLDialect.SQLITE)
            it.setConnection(DriverManager.getConnection(getProperty("db.jdbc.url")))
        }
    }
    single<DSLContext> { SQLiteDSL.using(get<Configuration>()) }
    single { MetricRegistry() }
    single { HashingStage(get()) }
    single { IdentifyingStage(get()) }
    single { StoreStage(getProperty("stage.store.path"), get()) }
    single { PersistStage(get(), get()) }
    single {
        IndexcellentEngine(
            listOf(
                get<HashingStage>(),
                get<IdentifyingStage>(),
                get<StoreStage>(),
                get<PersistStage>()
            ), get(), get()
        )
    }
}