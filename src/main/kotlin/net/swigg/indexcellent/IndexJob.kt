package net.swigg.indexcellent

import java.nio.file.Path
import java.util.*

data class IndexJob(
    val name: String,
    val path: Path,
    val uuid: UUID = UUID.randomUUID()
)