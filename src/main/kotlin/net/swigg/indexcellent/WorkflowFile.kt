package net.swigg.indexcellent

import com.google.common.collect.Multimap
import com.google.common.collect.MultimapBuilder
import net.swigg.indexcellent.stage.Stage
import net.swigg.nio.ByteBufferInputStream
import net.swigg.nio.FileChannelInputStream
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.file.attribute.BasicFileAttributes
import java.time.Duration
import kotlin.reflect.KClass

typealias StageDuration = Multimap<KClass<out Stage>, Duration>

class WorkflowFile(
    private val channel: FileChannel? = null,
    val attributes: BasicFileAttributes,
    val indexItem: IndexItem,
    val indexJob: IndexJob,
    val stageDuration: StageDuration = MultimapBuilder.linkedHashKeys().linkedListValues().build()
) : AutoCloseable {
    private val byteBuffer: ByteBuffer by lazy {
        ByteBuffer.allocate(minFileChannelSize).let {buffer ->
            channel?.read(buffer)
            buffer.flip()
            buffer.asReadOnlyBuffer()
        }
    }

    fun inputStream() = when {
        channel is FileChannel && channel.size() > minFileChannelSize -> FileChannelInputStream(channel)
        channel is FileChannel -> ByteBufferInputStream(byteBuffer.duplicate())
        else -> null
    }

    companion object {
        /**
         * Minimum size a file must be before it uses [FileChannelInputStream] instead of [ByteBufferInputStream].
         * Set to 2^23 bytes (8 MB)
         */
        const val minFileChannelSize = (1 shl 23) - 1
    }

    override fun close() {
        channel?.close()
    }
}