package net.swigg.indexcellent

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.Slf4jReporter
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import java.nio.file.Paths
import java.util.concurrent.TimeUnit

class IndexcellentApp : KoinComponent {
    private val log = mu.KotlinLogging.logger {}

    val engine by inject<IndexcellentEngine>()
    val metrics by inject<MetricRegistry>()

    fun start(args: Array<String>) {
        log.info("Started")
        engine.index(args[0], Paths.get(args[1]))

        Slf4jReporter.forRegistry(metrics)
            .convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS)
            .build()
            .report()
    }
}

fun main(args: Array<String>) {
    startKoin {
        properties(mapOf(
            "db.jdbc.url" to "jdbc:sqlite:file:indexcellent.db",
            "db.jdbc.driver" to "org.sqlite.JDBC",
            "stage.store.path" to Paths.get("/Users/dpsweigart/Desktop/indexcellent")
        ))
        modules(indexcellentKoinModule)
    }

    val app = IndexcellentApp()
    app.start(args)
}