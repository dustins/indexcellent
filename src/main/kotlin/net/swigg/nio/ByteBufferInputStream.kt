package net.swigg.nio

import java.io.IOException
import java.io.InputStream
import java.nio.ByteBuffer

/**
 * A [ByteBufferInputStream] obtains input bytes from a [ByteBuffer].
 *
 * [ByteBufferInputStream] is meant to be a simple wrapper around a [ByteBuffer] so that it can be used even when an
 * [InputStream] is required. This [InputStream] is a good option for smaller files that can comfortably fit into
 * memory and need to be read multiple times. For larger files [FileChannelInputStream] should be considered.
 *
 * @author Dustin Sweigart <dustins@swigg.net>
 */
class ByteBufferInputStream(buffer: ByteBuffer) : InputStream() {
    private val buffer: ByteBuffer = buffer.asReadOnlyBuffer()

    private var readLimit: Int = 0

    override fun available(): Int = buffer.remaining()

    override fun read(): Int {
        if (!buffer.hasRemaining()) {
            return -1
        }

        readLimit--
        return buffer.get().toInt()
    }

    override fun read(byteArray: ByteArray?, offset: Int, length: Int): Int {
        if (!buffer.hasRemaining()) {
            return -1
        }

        val remaining = Math.min(length, buffer.remaining())
        buffer.get(byteArray, offset, remaining)
        readLimit-= remaining
        return remaining
    }

    override fun markSupported(): Boolean = true

    override fun mark(readLimit: Int) {
        this.readLimit = readLimit
        buffer.mark()
    }

    override fun reset() {
        when {
            readLimit < 0 -> throw IOException("Resetting to invalid mark")
            else -> {
                val currentPosition = buffer.position()
                buffer.reset()
                readLimit = currentPosition - buffer.position()
            }
        }
    }
}