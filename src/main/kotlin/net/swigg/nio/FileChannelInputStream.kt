package net.swigg.nio

import com.google.common.base.Preconditions
import java.io.IOException
import java.io.InputStream
import java.nio.ByteBuffer
import java.nio.channels.FileChannel

/**
 * A [FileChannelInputStream] obtains input bytes from a [FileChannel]. It does this by sharding the [FileChannel] into
 * memory mapped [ByteBuffer]s as necessary.
 *
 * [FileChannelInputStream] is meant for reading streams of raw bytes under the following conditions:
 *      - overhead incurred from memory mapping a file makes sense because the stream will be read multiple times
 *      - number of bytes to be read is larger than could fit into a buffer
 *
 * For smaller files [ByteBufferInputStream] should be considered.
 *
 * @author Dustin Sweigart <dustins@swigg.net>
 */
class FileChannelInputStream(
    private val channel: FileChannel,
    shardSize: Int = Int.MAX_VALUE
) : InputStream() {
    private val shardSize = shardSize.coerceAtLeast(1).toLong()

    private var currentShard: Long = 0

    private var buffer: ByteBuffer = getBuffer(currentShard)

    private var mark: Pair<Long, Int> = Pair(0, 0)

    private var readLimit: Long = 0

    private fun shardOffset(shardIndex: Long) = shardIndex * shardSize

    private fun hasMoreShards(): Boolean {
        return (channel.size() - (shardOffset(currentShard) + buffer.position())) > 0
    }

    private fun getBuffer(shardIndex: Long): ByteBuffer {
        val channelSize = channel.size()
        val shardOffset = shardOffset(shardIndex)
        val size = Math.min(channelSize - shardOffset, shardSize)
        return channel.map(FileChannel.MapMode.READ_ONLY, shardOffset, size)
    }

    init {
        Preconditions.checkArgument(channel.isOpen)
    }

    override fun available(): Int {
        return when (hasMoreShards()) {
            false -> buffer.remaining()
            else -> {
                val channelSize = channel.size()
                val shardOffset = shardOffset(currentShard)
                return (channelSize - (shardOffset + buffer.position())).toInt().coerceAtMost(Int.MAX_VALUE)
            }
        }
    }

    override fun read(): Int {
        val bufferHasRemaining = buffer.hasRemaining()
        if (!bufferHasRemaining && !hasMoreShards()) {
            return -1
        }

        if (!bufferHasRemaining) {
            buffer = getBuffer(++currentShard)
        }

        readLimit--
        return buffer.get().toInt()
    }

    override fun read(byteArray: ByteArray, offset: Int, length: Int): Int {
        val bufferHasRemaining = buffer.hasRemaining()
        if (!bufferHasRemaining && !hasMoreShards()) {
            return -1
        }

        if (!bufferHasRemaining) {
            buffer = getBuffer(++currentShard)
        }

        val maxByteArrayLength = Math.min(length, byteArray.size - offset)
        val readLength = Math.min(maxByteArrayLength, buffer.remaining())
        val positionAtStart = buffer.position()
        buffer.get(byteArray, offset, readLength)

        val readBytes = buffer.position() - positionAtStart
        readLimit = readBytes.toLong()
        return readBytes
    }

    override fun markSupported(): Boolean = true

    fun mark(readLimit: Long) {
        this.mark = Pair(currentShard, buffer.position())
        this.readLimit = readLimit
    }

    override fun mark(readLimit: Int) {
        this.mark = Pair(currentShard, buffer.position())
        this.readLimit = readLimit.toLong()
    }

    override fun reset() {
        when {
            readLimit < 0 -> throw IOException("Resetting to invalid mark")
            else -> {
                readLimit = (shardOffset(currentShard) + buffer.position()) - (shardOffset(mark.first) + mark.second)
                buffer = getBuffer(mark.first)
                buffer.position(mark.second)
            }
        }
    }

    fun restart() {
        currentShard = 0
        getBuffer(currentShard)
        mark = Pair(0, 0)
        readLimit = 0
    }

    override fun close() {
        super.close()
        currentShard = -1
        buffer = ByteBuffer.allocate(0)
        mark = Pair(0, 0)
        readLimit = -1
    }
}