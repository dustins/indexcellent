//package net.swigg.exposed
//
//import com.google.common.hash.HashCode
//import org.apache.tika.mime.MediaType
//import org.jetbrains.exposed.sql.*
//import java.nio.file.Path
//import java.nio.file.Paths
//import java.time.Duration
//import java.time.Instant
//
//fun Table.instant(name: String): Column<Instant> = registerColumn(name, InstantColumnType(true))
//fun Table.duration(name: String): Column<Duration> = registerColumn(name, DurationColumnType())
//fun Table.hash(name: String): Column<HashCode> = registerColumn(name, HashColumnType())
//fun Table.path(name: String): Column<Path> = registerColumn(name, PathColumnType())
//fun Table.mediaType(name: String): Column<MediaType> = registerColumn(name, MediaTypeColumnType())
//
//class MediaTypeColumnType : ColumnType() {
//    private val delegate = VarCharColumnType()
//
//    override fun sqlType(): String = delegate.sqlType()
//
//    override fun nonNullValueToString(value: Any): String {
//        return when (value) {
//            is MediaType -> delegate.nonNullValueToString(value.toString())
//            else -> delegate.nonNullValueToString(value)
//        }
//    }
//
//    override fun notNullValueToDB(value: Any): Any {
//        return when (value) {
//            is MediaType -> delegate.notNullValueToDB(value.toString())
//            else -> delegate.notNullValueToDB(value)
//        }
//    }
//
//    override fun valueFromDB(value: Any): Any {
//        return delegate.valueFromDB(value).let {
//            when (it) {
//                is String -> MediaType.parse(it)
//                else -> error("Unexpected value of type String: $value of ${value::class.qualifiedName}")
//            }
//        }
//    }
//}
//
//class PathColumnType : ColumnType() {
//    private val delegate = VarCharColumnType()
//
//    override fun sqlType(): String = delegate.sqlType()
//
//    override fun valueToString(value: Any?): String {
//        return when (value) {
//            is Path -> delegate.valueToString(value.toString())
//            else -> delegate.valueToString(value)
//        }
//    }
//
//    override fun nonNullValueToString(value: Any): String {
//        return when (value) {
//            is Path -> delegate.nonNullValueToString(value.toString())
//            else -> delegate.nonNullValueToString(value)
//        }
//    }
//
//    override fun notNullValueToDB(value: Any): Any {
//        return when (value) {
//            is Path -> delegate.notNullValueToDB(value.toString())
//            else -> delegate.notNullValueToDB(value)
//        }
//    }
//
//    override fun valueFromDB(value: Any): Any {
//        return when (value) {
//            is String -> Paths.get(value)
//            else -> super.valueFromDB(value)
//        }
//    }
//}
//
//class HashColumnType : ColumnType() {
//    private val delegate = VarCharColumnType()
//
//    override fun sqlType(): String = delegate.sqlType()
//
//    override fun nonNullValueToString(value: Any): String {
//        return when (value) {
//            is HashCode -> delegate.nonNullValueToString(value.toString())
//            else -> error("Unexpected value of type ${value::class.qualifiedName}")
//        }
//    }
//
//    override fun notNullValueToDB(value: Any): Any {
//        return when (value) {
//            is HashCode -> delegate.notNullValueToDB(value.toString())
//            else -> delegate.notNullValueToDB(value)
//        }
//    }
//
//    override fun valueFromDB(value: Any): Any {
//        return delegate.valueFromDB(value).let {
//            when (it) {
//                is String -> HashCode.fromString(it)
//                else -> error("Unexpected value of type String: $value of ${value::class.qualifiedName}")
//            }
//        }
//    }
//}
//
//class DurationColumnType : ColumnType() {
//    private val delegate = LongColumnType()
//
//    override fun sqlType(): String = delegate.sqlType()
//
//    override fun nonNullValueToString(value: Any): String {
//        return when(value) {
//            is Duration -> value.toMillis().toString()
//            else -> delegate.nonNullValueToString(value)
//        }
//    }
//
//    override fun notNullValueToDB(value: Any): Any {
//        return when (value) {
//            is Duration -> value.toMillis()
//            else -> delegate.notNullValueToDB(value)
//        }
//    }
//
//    override fun valueFromDB(value: Any): Any {
//        return delegate.valueFromDB(value).let {
//            when (it) {
//                is Long -> Duration.ofMillis(it)
//                else -> error("Unexpected value of type Long: $value of ${value::class.qualifiedName}")
//            }
//        }
//    }
//}
//
//class InstantColumnType(time: Boolean) : ColumnType() {
//    private val delegate = DateColumnType(time)
//
//    override fun sqlType(): String = delegate.sqlType()
//
//    override fun nonNullValueToString(value: Any): String {
//        return delegate.nonNullValueToString(value)
//    }
//
//    override fun valueFromDB(value: Any): Any {
//        return delegate.valueFromDB(value)
//    }
//
//    override fun notNullValueToDB(value: Any): Any {
//        return delegate.notNullValueToDB(value)
//    }
//}