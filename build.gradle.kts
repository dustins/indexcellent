import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "net.swigg"
version = "0.0.1-SNAPSHOT"

plugins {
    java
    kotlin("jvm") version "1.3.21"
    idea
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
}

repositories {
    mavenLocal()
    jcenter()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.1.+")

    compile("org.koin:koin-core:2.0.0-rc-1")
    compile("io.dropwizard.metrics:metrics-core:4.0.+")
    compile("com.google.guava:guava:27.0.1-jre")
    compile("org.apache.tika:tika-core:1.20")

    compile("org.xerial:sqlite-jdbc:3.25.+")
    compile("org.jooq:jooq:3.11.+")
    compile("org.jooq:jooq-meta:3.11.+")
    compile("org.jooq:jooq-codegen:3.11.+")
    compile("javax.xml.bind:jaxb-api:2.3.1")
    compile("javax.xml:jaxb-impl:2.1")

    // logging
    compile("ch.qos.logback:logback-classic:1.2.+")
    compile("io.github.microutils:kotlin-logging:1.6.+")

    // testing
    testCompile("junit", "junit", "4.12")
}

sourceSets {
    main {
        java {
            srcDir("src/main/generated")
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    wrapper {
        distributionType = Wrapper.DistributionType.ALL
    }
}

task("generateJooq", JavaExec::class) {
    main = "org.jooq.codegen.GenerationTool"
    classpath = sourceSets["main"].runtimeClasspath
    args("src/main/resources/jooq-config.xml")
}